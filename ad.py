#!/usr/bin/env python3
"""
Module Docstring
"""

from time import sleep
import psutil
import subprocess
import getpass
import argparse
import configparser
from mutagen.mp3 import MP3
from mplayer import Player
from pathlib import Path
#import os
import glob
from datetime import timedelta

__author__ = "Philipp Häusele"
__version__ = "0.1.0"
__license__ = "MIT"

_sleepTime=0
_shutdownTime=0
_audioBookMode=0
_audiobook=""
_shuffle=False
_myUser=''
arg={}

def getChapterLength(path):
    print(path)
    audio = MP3( glob.glob(path)[0])
    return eval ('audio.info.length / 60 / 15')


def readConfig(title):
    config = configparser.ConfigParser()
    config.read('/home/fed_phil/.config/play-ab/python.ini')
    title = str(title)
    arg._cd = config[title]['cd']
    arg._chapter = config[title]['chapter']
    arg._part = config[title]['part']

def writeConfig(title):  
    config = configparser.ConfigParser()
    print('writing cd: ' + str(arg._cd))
    config[title] = {'cd' : str(arg._cd),
                       'chapter' : str(arg._chapter),
                       'part' : str(arg._part)}
    with open('/home/fed_phil/.config/play-ab/python.ini', 'w+') as configfile:
       config.write(configfile)


def killSound():
    programs = ["vlc", "spotify", "Firefox", "Soundnode" ]
    subprocess.run("mpc -p 7777 pause")
    for proc in psutil.process_iter():
    # check whether the process name matches
        if proc.name() in programs:
            proc.kill()

def getUser():
    return getpass.getuser()

def getUserPath():
    if getUser() is "phi1":
        return "phi1/"
    else:
        return ""

def parse():
    global arg
    parser = argparse.ArgumentParser(description='Short sample app')

    #parser.add_argument('-a', action="store_true", default=False)
    #parser.add_argument('-b', action="store", dest="b")
    #parser.add_argument('-c', action="store", dest="c", type=int)

    parser.add_argument('-w', dest="_sleepTime", type=int)
    parser.add_argument('-s', dest="_shutdowndownTime", default=0, type=int)
    parser.add_argument(dest="_audiobook", default='k')
    parser.add_argument('-cd', dest='_cd', type=int, default=1)
    parser.add_argument('-ch', dest='_chapter', type=int, default=1)
    parser.add_argument('-pa', dest='_part', type=int, default=0)


    arg = parser.parse_args()
    

def nlp():
    _title = "NLP"
    global _shuffle
    _shuffle = True
    path = '''Nextcloud/Audio/NLP\ fresh-up/*'''
    return {'path': path, 'title': _title}


def kang():
    _title = "KÄNGURU"
    path = 'Nextcloud/Audio/Audiobooks/Marc\ Uwe\ Kling/*/*'
    global _shuffle
    _shuffle = True
    return {'path': path, 'title': _title}


def quali():
    _title = "QUALITYLAND"
    path = 'Nextcloud/Audio/Audiobooks/QualityLand/*'
    if (arg._cd > 0 and arg._cd < 8):
        path = 'Nextcloud/Audio/Audiobooks/QualityLand/QualityLand0' + str(arg._cd) + '.mp3'
    return {'path': path, 'title': _title}

def allg():
    _title = "ALLGEMEINBILDUNG"
    global _shuffle
    _shuffle = True
    return {'path': 'Nextcloud/Audio/Music/Allgemeinbildung/*/*', 'title': _title}

def was():
    _title = "WAS_IST_WAS"
    _myuser = getUserPath()
    #path = 'Nextcloud/Audio/Audiobooks/' + subprocess.check_output('ls -1 "/home/' + _myuser + '''Nextcloud/Audio/Audiobooks/Was.ist.Was?"|shuf -n1 | sed 's/ /\\ /g' ''', shell=True)
    list = subprocess.Popen(['ls', '-1',
    '/home/' + _myuser + 'Nextcloud/Audio/Audiobooks/Was.ist.Was?'], stdout=subprocess.PIPE)
    

    path = 'Nextcloud/Audio/Audiobooks/Was.ist.Was?/' + subprocess.check_output(['shuf',
    '-n1'], stdin=list.stdout).decode("utf-8") + '/*'
    path = path.replace('\n','').replace(' ', '\\ ')
    print('Path: ' )
    return {'path': str(path), 'title': _title}

def harry():
    _title = "HARRY"
    if(arg._cd == 1 and arg._chapter == 1 and arg._part == 0):
        print('reading config...')
        readConfig(_title)
    path = r"Nextcloud/Audio/Audiobooks/Harry Potter/"
    
    if(int(arg._cd) > 0 and int(arg._cd) < 8):
        path += str(arg._cd) + '*/'
    if(int(arg._chapter) > 0):
        if(int(arg._chapter) < 10 and '0' not in str(arg._chapter)):
            arg._chapter = '0' + str(arg._chapter)
        path += '' + str(arg._chapter) 
    path += r'*'
    return {'path': path, 'title': _title}

def secondChapter():
    path = r"/home/Nextcloud/Audio/Audiobooks/Harry Potter/"
    chapter = int(arg._chapter) + 1
    if(int(arg._cd) > 0 and int(arg._cd) < 8):
        path += str(arg._cd) + '*/'
    if(int(arg._chapter) > 0):
        if(int(chapter) < 10 and '0' not in str(chapter)):
            chapter = '0' + str(chapter)
        path += '' + str(chapter) 
    path += r'*'
    return path

def getNextPart(path):
    if "Marc" in path:
        return
    global arg
    parts = getChapterLength(path)
    if(int(arg._part) <= parts):
        arg._part = int(arg._part) + 1
    else:
        arg._chapter = int(arg._chapter) + 1
        arg._part = 0



def getAudioBookPath():
    _myUser = getUserPath()
    audiobooks = {
        "k" : kang,
        'n' : nlp,
        'q' : quali,
        'a' : allg,
        'w' : was,
        'h' : harry
    }
    func = audiobooks.get(arg._audiobook,nlp)
    pathObject = func()
    
    pathObject['path'] = "/home/" + _myUser + pathObject['path']
    return pathObject

def play(path):
    startTime = int(arg._part) * 15
    _command = "mplayer "
    if(_shuffle):
        _command += '-shuffle '
    time = str(timedelta(minutes=int(startTime)))
    print(path['path'])
    if "Harry" in path['path']:
        _command += '' + '-ss 0' + time + ' ' + path['path'].replace(' ', '\ ') + ' ' + secondChapter().replace(' ', '\ ') + ' | sed 1,2d'
        getNextPart(path['path'])
        writeConfig(path['title'])
        subprocess.run(_command, shell=True)
    else:
        _command += path['path']
        subprocess.run(_command, shell=True)

def shutdown(time):
    subprocess.run("shutdown -h +" + str(time), shell=True)
    subprocess.run("(sleep 2m && gnome-screensaver-command -l &)", shell=True)

def main():
    """ Main entry point of the app """
    _myUser = getUser()
    print("hello " + _myUser)
    parse()
    if(int(arg._shutdowndownTime) > 0):
        shutdown(int(arg._shutdowndownTime))
    play(getAudioBookPath())
    

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()